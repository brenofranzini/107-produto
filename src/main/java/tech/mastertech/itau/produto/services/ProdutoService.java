package tech.mastertech.itau.produto.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import tech.mastertech.itau.produto.dto.Categoria;
import tech.mastertech.itau.produto.dto.Produto;
import tech.mastertech.itau.produto.repositories.ProdutoRepository;

@Service
public class ProdutoService {

	@Autowired
	private ProdutoRepository produtoRepository;

	public Iterable<Produto> getProdutos() {
		return produtoRepository.findAll();
	}

	public Produto setProduto(Produto produto) {
		produtoRepository.save(produto);
		return produto;
	}

	public Produto getProduto(int id) {
		Optional<Produto> produto = produtoRepository.findById(id);
		if (produto.isPresent()) {
			return produto.get();
		}
		return null;
	}

	public Produto atualizaProduto(int id, double valor) {

		if (valor <= 0) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Valor inválido - deve ser um positivo");
		}

		Produto produto = getProduto(id);
		if(produto != null)
		{
			produto.setValor(valor);
			produtoRepository.save(produto);
		}

		return produto;
	}
	
	public Iterable<Produto> getProdutosCategorias(Categoria categoria) {
		return produtoRepository.findAllByCategoria(categoria);
	}
	
	public void deletaProduto(int id) {

		Produto produto = getProduto(id);
		if(produto != null)
		{
			produtoRepository.delete(produto);
		}
	}
}
