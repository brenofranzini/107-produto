package tech.mastertech.itau.produto.dto;

public enum Categoria {
	CONDIMENTO,
	LIMPEZA,
	ALCOOL,
	LATICINIO;
}
