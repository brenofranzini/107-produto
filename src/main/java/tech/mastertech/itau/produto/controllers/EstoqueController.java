package tech.mastertech.itau.produto.controllers;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import tech.mastertech.itau.produto.dto.Categoria;
import tech.mastertech.itau.produto.dto.Produto;
import tech.mastertech.itau.produto.services.ProdutoService;

@RestController
@RequestMapping("/estoque")
public class EstoqueController {
	private Produto retorno;
	List<Produto> produtos;

	@Autowired
	private ProdutoService produtoService;

	@GetMapping("/produtos")
	public Iterable<Produto> getProdutos(@RequestParam(required = false) Categoria categoria) {
		if (categoria == null) {
			return produtoService.getProdutos();
		}

		Iterable<Produto> produtos = produtoService.getProdutosCategorias(categoria);

		if (produtos == null) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
		return produtos;
	}

	@ResponseStatus(HttpStatus.CREATED)
	@PostMapping("/produto")
	public Produto setProduto(@RequestBody Produto produto) {
		return produtoService.setProduto(produto);
	}

	@GetMapping("/produto/{id}")
	public Produto getProduto(@PathVariable int id) {
		retorno = produtoService.getProduto(id);
		if (retorno == null)
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);

		return retorno;
	}

	@PatchMapping("/produto/{id}")
	public Produto atualizaProduto(@PathVariable int id, @RequestBody Map<String, Double> valor) {
		retorno = produtoService.atualizaProduto(id, valor.get("valor"));
		if (retorno == null)
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		return retorno;
	}
	
	@ResponseStatus(HttpStatus.ACCEPTED)
	@DeleteMapping("/produto/{id}")
	public void deletaProduta(@PathVariable int id) {
		produtoService.deletaProduto(id);
	}

//	Atualizar o valor de um produto
	/*
	 *
	 * { "id": 2, "nome": "Corote Teste", "categoria": "LIMPEZA", "descricao":
	 * "Corotinho sabor alvejante - deixa doido e limpa pia", "valor": 4.6 }
	 */
}
